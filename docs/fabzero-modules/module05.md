# 5. Dynamique de groupe et projet final 

Cette semaine nous avions travaillé en groupe sur le projet final de fin d'année. 

## 5.1 Analysis and design 

Quelques semaines auparavant nous devions s'inspirer des projets Fablab et Frugal sciences pour faire notre permier arbre à problèmes et solutions au sujet d'une problèmatique présentée.  
Ce travail peut se faire par paire (en duo) ou individuellement. J'ai choisi de travailler avec [Kawtar](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/Kawtar.zaouia/) sur le Foldscope. 

Le Foldscope est un petit microscope en papier d'où vient son nom "**folding-paper microscope**". Ce microscope est en effet en papier pliable. Son coùt de production est minime; moins de deux dollars, il est donc premièrement destiné aux pays du tiers-monde. Il est d'une réelle efficacité car il permetun grossissement jusqu'à même 2000 fois. 

Ce microscope est une réelle révolution car plus tard il servira à depister des maladies, ce qui est très utile en période d'épidémie car un vrai microscope est un outil plus cher, moins accessible et très peu maniable surtout sur le terrain. 

Ce petit microscope peut même être jettable et donc empêche toute contamination comme pour des pays où les produits de décontamination par milliers sont indisponibles et où les microscopes seraient alors agents véhiculateurs de maladies ou de microbes si pas désinfectés correctement. 

![](images/tree1.png)

ce microscope est en réalité fait d'un petit papier, avec des composants optiques imprimés en 3D, un éclairage LED, une lentille. Il faut y insérer une lame ou une lamelle pour fonctionner. 

![](images/foldscope.png)


## 5.2  Group formation and brainstorming project 

Comme avant chaque cours, nous devions nous "préparer" [before class](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/class-website/fabzero-modules/team-dynamics-final-project/). Pour cela, nous devions regarder cette [vidéo](https://www.youtube.com/watch?v=9KIlK61RInY). Après avoir visionné cette vidéo, j'ai donc compris que je devais penser à une problèmatique à laquelle je connaitrais une éventuelle solution mais aussi dont les conséquences sont claires et évidentes. 
Nous devions penser à une problèmatique qui nous touchait personnellement. De plus, il fallait apporter un objet qui était en lien avec notre problèmatique. 

J'ai donc décidé de prendre une  voiturette pour enfant verte. Cette voiture représentait globalement le **changement climatique**; pour être plus précise, je voulais parler de la **pollution de l'air** dûe aux combustibles fossiles, l'émission de gaz à effet de serre, le rejet grandiose de CO2. 

![](images/voiturette.png)


## 5.2.1 Fomation des groupes 

Nous avons commencé par mettre tous nos objets sur le sol afin qu'ils soient visibles pour tout le monde. On pouvait observer des gourdes, plante fannée, pelure de banane, plastique, emballage aluminium, tasse en plastique Starbucks, jouet microscope, ...

Ensuite, après avoir jeté un coup d'oeil sur les objets de chacun d'entre nous, nous devions nous placer chacun près de nos objets et nommer notre objet où expliquer ce qu'il est. De plus, nous devions se mettre en groupe avec des personnes qui avaient un objet que l'on pensait être similiare ou qui avait un lien avec le notre. 

Je me suis alors mise en premier avec [Virginie](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/virginie.louckx/) qui avait elle des clefs de voiture pour représenter sa problèmatique. Par la suite, nous nous sommes dirigés vers [Aymane](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/aymane.taifour/) qui avait pris une carte de transport de métro, tram, bus. [Lorea](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/lorea.latorre/) nous a ensuite rejoint, elle possédait une pièce de monnaie. Enfin, [Matthew](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/matthew.doyle/) nous a rejoind et il possédait un emballage plastique d'une yaourt de soja. 

Voilà, notre groupe est enfin formé !! 

![](images/obj.png)



## 5.2.2 Partage de nos problèmatiques

Nous nous sommes présentés dans un premier temps. Ensuite, nous avons chacun expliqué notre problématique et ce que représentait l'objet dans celle-ci. 

1. **Les clefs**:
Elles représentent un peu la même problématique que la mienne, c'est-à-dire la pollution énegétique. En prenant les clefs de sa voiture, Virginie souhaite parler  des voitures, des transports de sociétés et donc des gens qui profitent de cet "avantage" accordé par leur société et qui ne se rendent pas comptent de l'impact très néfaste que ça a sur l'air. 

2. **La carte de transport**: 
Ici, le but était d'illustrer la problématique des coûts en énergie des transports. Les sources d'énergie les plus utilisées sont l'essence et le gazole alors que les biocarburants eux couvrent qu'une minime partie d'énergie utilisée. Dans cela, les transports que ce soit train, voiture, bus, car, métro, avion etc. puisent énormément d'énergie. Le problème est que ces déplacements ne sont pas toujours indispensables et parfois représentent un "gaspillage énergétique" c'est-à-dire que l'on utilise mal ou inutilement l'énergie. mais cela a un coût... 

3. **La pièce de monnaie**:
Celle-ci représente la problématique récente et qui touche tout le monde, qui est le contrôle des industries. En effet, Lorea a expliqué qu'elle désirait parler de l'avenir des banques, la suppression de l'argent liquide. Ainsi, notre argent ne serait plus contrôlée par nous même, nous ne posséderons plus notre argent physiquement. C'est donc, selon elle, une sorte de contôle de nos "vies" ou en tout cas de nos salaires. 

4. **Le plastique emballage de yaourt soja**:
Le but de Matthew était de parler de la pollution dûe aux transports. Plus précisémment les transports de longues distances comme celui du soja par exemple, qui vient de l'autre bout de monde et qui parcourt énomément de pays, continents même. Mais qui dit transports dit émission de CO2  mais aussi de plein de polluants atomsphérique !!! 
De plus, son but était de parler des multiples impacts environnementaux conséquents comme la déforestation, déforestations des sols, sécheresse etc,. 

## 5.1.4 Votons pour 1 seule problématique
Maintenant, passons à la partie plus pratique de cette séance. En effet, nous devions sur un panneau, mettre un maximum de mots, de mots-clefs, de phrases ou d'idées-clefs par rapport à nos différentes problématiques. 
Au final, nous avions 13 mots. D'autres groupes avait 11,15, ou même mots etc.

L'excercice se poursuivait par une session où nous devions rassembler nos problématique et parler d'une problématique commune qui nous intéresserait à aborder tous ensemble. Pour cela, sur une feuille, nous devions mettre sur une feuille notre motivation à travailler sur chacune des 5 problématiques. La problématique qui récoltait un maximum de votes, était celle qui était tiré du "lot" afin d'être traitée en groupe. 

![](images/numero.png) ![](images/vote.png)

And the winner isssssss ... MA PROBLÉMATIQUE, nous allons donc aborder le sujet de la pollution liée aux transports. 


## 5.2.3 Moi à ta place... 

Ensuite, nous devions faire un "jeu" qui consiste à prendre la parole en commençant par "**moi à ta place**" et finir par son idée. Le but ici est de susciter un maximum d'idées ou de contre-idées pour permettre un maximum de diversité dans nos idées. De plus, une règle mise en place consistait à créer une conversation continue, sans arrêt. Nous devions chacun poursuivre l'idée de l'autre sans arrêt. Ici, j'ai décidé de noter les différentes idées des différents membres de mon groupe qui était un réel challenge car ils parlaient vite, parfois leurs idées n'était pas très claires, mais le plus difficile était de mettre un mot ou plusieurs mots-clefs qui résument l'idée d'un membre. 

Voici le résultat de cette petote session sur cette feuille: 

![](images/moiataplace.png)

## 5.2.4 Echange entre différents groupes 

Last but not least, une personne de notre groupe devait prendre un rôle de porte parole auprès de deux groupes afin de récolter les idées des autres à propos de notre sujet. Dans notre groupe nous avions désigné Virginie qui devait expliquer notre problématique et noter toutes les idées que cela peut susciter aux autres groupes et auxquelles nous n'aurions pas pensé. 

C'est d'ailleurs un exercice très intéressant car nous avions remarqué qu'il y avait certaines problématique comme la polution sonore à laquelle nous n'aurions jamais pensé et pourtant c'est quelque chose dont on parle souvent dernièrement. De plus, elle correspond totalement à notre sujet global : **la pollution**. 

![](images/idee1.png)

De plus, deux portes-paroles de deux autres groupes se sont présentés dans le notre pour nous permettre de leur apporter plus d'idées. La première porte-parole, [Chiara](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/chiara.castrataro/),  avait comme problématique "vivre en harmonie avec les océans". Ensuite c'était au tour de [Karl](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/karl.preux/), porte-parole du groupe qui traitait le recyclage. 


## 5.2.5 Fin du cours 

Finalement, cette séance était un plaisir car elle m'a appris de communiquer avec des étudiants que je connais pas, chose que je n'ai absolument pas l'habitude de faire. Elle m'a permis de vaincre ma "timidité" et d'oser travailler avec d'autres personnes que mes amis mais aussi de découvrir d'autres personnes qui partagent le même cours que moi et une problématique plus ou moins "commune". 

Lors d'une dernière participation à un dernier cercle où nous avions tous le droit de donner un mot qui décrivait notre séanec, j'ai décidé de dire "étonné"  car c'est exactement le sentiment que j'avais à ce moment-là. 



## 5.3 Group dynamics 
## 5.3.1 Suite sur la dynamique de groupe 

Durant cette séance, nous avons continué notre travail sur la dynamique de groupe avec Chloé Crokart de Collectiv-a. Elle nous a expliqué qu'elle encadre plusieurs groupes dans différentes milieux de travail et son but était de nous donner des outils et des trucs-astuces pour nous permettre de mieux gérer l'entente, le travail, et les obstacles dans le groupe. De plus, son but est aussi de nous aider à se diviser des tâches et de travailler en groupe le plus efficacement possible et d'économiser plus de temps possible. 


## 5.3.2 Excercice du "oui"

Pour ce premier exercice, quand la réponse est "oui" nous devions nous lever. 
Tout d'abord, la première question de Chloé était pour qui d'entre nous le travail en groupe est un défi, quelque chose avec laquelle nous pavons du mal  ou pour qui d'entre nous le travail en groupe est un plaisir, quelque chose avec laquelle nous sommes à l'aise. 

Ensuite, s'en est suivis plusieurs questions du même style. 

## 5.3.3 Méthodes de gestion de temps de paroles dans le groupe 

Nous avons ensuite parlé de la dominance par la parole; c'est-à-dire qu'il y a parfois des personnes qui s'expriment beaucoup plus que d'autres dans certains groupes et des personnes qui ne s'expriment quasiment pas. 

Pour cela, il nous fallait désigner des "outils" des méthodes que l'on peut mettre en place pour permettre une bonne gestion du temps de paroles et d'un bon partage de ce temps entre les différents membres qui constituent un groupe : 

- Bâton de parole 
- Chronomtérer le temps de parole et donc avoir une limite de temps de parole
- Tour de parole, c'est-à-dire que chacun a son tour pour pouvoir parler 
- La méthode du doigt 

## 5.3.4 L'art de décider 

Il existe différentes façons de décider ou même de réaliser des tâches: 

- en collectif 
- seule donc un membre de groupe 
- ne pas prendre de décision mais donc c'est aussi une décision 

## 5.3.5 Déroulement des réunions ou des meetings 

Chloé nous a expliqué qu'il était important et intéréssant de commencer par des météo d'entrées. Ensuite, il est important de répartir les différentes rôles de chacun avant de commencer la réunion et de poursuivre avec l'ordre du jour. 

En fin de réunion, il est toujours considérable de répartir  les tâches, de planifier une sorte d'agenda pour le travail à organiser et à faire dans les prochains jours ainsi qu'une petite météo de sortie. 

Par rapport aux différentes rôles, il nous était suggéré de désigner 
- une personne chargée de la communication dans le groupe  : Mariam 
- un/une animateur/animatrice : Matthew 
- un/une secrétaire : Lorea
- une personne chargée de la gestion du temps ou de la parole de chacun durant les réunions

Nous avons aussi décidé que les décisions qui incluent quelconque outils dont l'un d'entre nous aurait suivi sa formation, c'est à cette personne que revient la décision finale. 

## 5.3.6 Méthodes de décision 

Il existe aussi différentes méthodes pour décider :
- Consensus
- Tirage au sort 
- Votes classés 
- Votes pondérés 
- Jugement majoritaire 
- Votes sans objection 
- Chemin de décision 
- Température check 

Nous avons ensemble pris la décision d'adopter la méthode du vote pondéré pour les décisions importantes et pour celles moins conséquentes nous choisirons grâce au Température check. 

## 5.3.7 Mes outils choisis à développer 

De plus, il faut développer 3 outils qui nous parle le plus. J'ai choisi de parler des 3 outils suivants: 

1. **Tâches à accomplir seuls**  

Je décide de développer cet outil en premier car dans un premier temps j'ai plutôt du mal à travailler en groupe alors pour moi les tâches à réaliser seule sont une bonne méthode de travailler. Bien évidemment, il y a des tâches qui doivent être faites en groupe, en duo ou trio. En bref, je pense que le principal est de privilégier l'efficacité, c'es-à-dire faire un maximum de tâche en un minimum de temps. Pour cela, il est plus intéressant par exemple que j'effectue une tâche pendant que monb camarade se charge d'une autre et qu'un autre membre travaille sur une autre tâche en parallèle. C'est selon moi un réel gain de temps. 

Petite anecdote, j'ai déjà travaillé dans un groupe une l'un des membres voulait vraiment qu'on se charge de toutes les tâches ensemble. c'était pas vraiment la meilleure idée car finalement par rapport aux autres groupes, nous n'avancions très lentement. Dans certains cas, nous étions en appel et il n'y avait qu'une personne d'entre nous qui travaillait seule au final. Je trouvais que c'était une très grande perte de temps car pendant ce temps où les autres membres du groupes ou moi ne faisions rien, nous aurions pu avancer sur d'autres tâches en parallèle. 

2. **Température check** 

Selon moi, c'est LE meilleur outil de travail en groupe pour passer un vote. En effet, dans les décisions en groupe à prendre, il est parfois difficile de se décider soi-même individuellement, alors quand une personne qui est dubitative partage ses doutes et ses raisons d'être indécis avec les autres groupes ça peut devenir "contagieux" dans le sens où cela peut directement faire douter d'autres membres du groupe. 

Cette méthode permet dans un premier temps de se décider soi-même, puis permettre des facilités et un gain de temps au groupe car si personne ne s'oppose, il n'y a même pas besoin d'agrumenter, on peut directement passer à autre chose. 

3. **Ordre du jour** 

Il est important d'établir selon moi un ODJ avant même un meeting ou une réunion pour qu'il y ait une sorte de suite logique pendant la réunion et que personne ne soit perdu. 

Encore une fois, d'un point de vue efficacité et rapidité c'est important. Il arrive parfois que certaines personnes tournent autour d'une problématique et d'une idée pendant longtemps. C'est pour cela que je trouve qu'un ordre du jour est un important à mettre en place car cela permet d'avoir une vue d'ensemble sur tout ce qui doit être discuté et donc éviter de perdre du temps sur certains détails; ça permet donc une bonne gestion du temps. 

Mais aussi, il arrive qu' après un meeting on se souvient qu'il y avait telle ou telle chose à discuter, parfois même pendant un meeting et donccela ne fait que ralonger un meeting ou même faire partir les idées dans tous les sens. C'est donc pour ça qu'un ordre du jour établi au préalable est un réel coup de pouce pour garder tous les membres focus sur les différents points à survoler durant la réunion. 
























