
# 2. Conception Assistée par Ordinateur (CAO)

Cette semaine, nous avons découvert  différents logiciels de conceptions assistée par ordinateur (CAO). Ces outils permettent de créer ou de réaliser certaines modélisations et manipulations géométriques d'objets. Ils peuvent être utile en vue d'une fabrication, de tester virtuellement des outils afin de les fabriquer. 


## 2.1 Les 3 logiciels principalement choisis


Avec plusieurs étudiants, nous avons décidé de chacun travailler sur un logiciel différent et de mettre en commun nos différences expériences et permettre à tout le groupe de savoir manipuler un minimum chaque logiciel proposé pendant le cours. 

- **Openscad**
- **FreeCad**
- **Inkscape**

En réalité, il en existe plein d'autres comme autoCAD, tinkerCAD,... Voici quelques tutos que j'ai découvert à propos des autres logiciels que j'ai trouvé. 

- https://www.youtube.com/watch?v=c71lOSH6FWw
- https://www.youtube.com/watch?v=8832VZQAHsI
- https://www.youtube.com/watch?v=gNpDZt7k1hs

Personellement, je n'ai pas testé [Inkscape](https://inkscape.org) mais j'ai regardé un petit [tutoriel](https://www.youtube.com/watch?v=GSGaY0-4iik) pour en savoir un peu plus sur sa manipulation. 

### A. OpenSCAD 

Pour installer ce logiciel il suffit d'appuyer [ici](https://openscad.org). Il permet la réalisation de modèles en 3D à partir de codes dans l'édteur. Pour ma part, j'ai d'abord testé ce logiciel à partir du code d'un autre élève. C'est d'ailleurs un de ses avantages, il est possible de partager les différents codes de nos réalisations. Ces codes peuvent aussi être modifiés à leur tour. 

De mon côté ce n'est pas le logiciel que j'ai le plus utilisé mais c'est celui qui m'a paru le plus facile à manipuler. En effet, avec un groupe d'étudiants on souhaitait construire une catapulte à partir de différntes pièces attribués à chacun d'entre nous afin de respecter la condition de la flexibilité. Ma pièce était une sorte de cylindre creux dans lequel il fallait insérer un autre cylindre plus long pour créer une sorte de petit montage de soutien de la cuillère (mini engrenage).


![catapulte](images/esquissecata.png)

Avant tout, sur OpenSCAD il existe différentes commandes qu'il serait intéressant de maitriser avant de commencer les manipulations; ces fonctions sont accessible sur cette [cheat sheet](https://openscad.org/cheatsheet/).
Pour réaliser ce petit cylindre  sur OpenSCAD, j'ai en fait écrit un code qui comporte un cylindre (le cylindre basique) duquel je soustrait un autre petit cylindre(le creux) que j'ai ensuite translaté pour que le creux début à partir d'une des surfaces planes de mon cylindre. 
De plus, j'ai multiplié par une variable que j'ai nommé "side" de sorte à ce que je puisse agrandir ou réduire la taille de ma forme géométrique sans devoir modifier les valeurs dans chaque variables. 

![](images/catapulte.png)

Ensuite, une fois mon code crée, j'ai enregistré ce code dans mes fichiers. Il est très important de se souvenir d'enregistrer sans cesse les étapes du code pendant sa réalisation, de sorte à ce qu'on ne perde pas tout en cas de problème ou d'un petit beug sur le pc. 

Finalement voilà le code que j'ai utilisé : 

~~~
//http://www.graphicsmagick.org/download.html
$fn=100;
side= 1;
difference(){
cylinder(3*side,5*side,5*side); translate([0,0,-1*side]) cylinder(2*side,2*side,2*side);}
~~~

[Sur ce lien](https://www.youtube.com/watch?v=VHUWV_Ao9Ak&t=112s) il y a un petit tuto que j'ai rapidement visionné sur les différentes manipulations sur OpenSCAD pour débutants.

### B. FreeCAD 

[FreeCAD](https://www.freecad.org) est le logiciel que j'ai téléchargé en premier (mais il m'a tellement fait galérer que j'ai abandonné lol). Par contre, selon moi, c'est le plus amusant parce qu'il n'utilise pas de code pour sa construction mais il s'agit plutôt d'esquisses à partir de vecteurs. Ainsi, pour l'utiliser, j'ai commencé par regarder ce petit [tuto](https://www.youtube.com/watch?v=SPFoXfEAufw) rapide. 

C'est simple, ici, nous débutons par la réalisation d'un modèle 2D pour enfin aboutir à un modèle 3D. De plus, ici on peut voir que j'ai crée un cylindre creux mais il est creux des deux côtés. Ce n'était pas ce qu'il me fallait et je ne trouvais pas comment refermer une des deux surface plane du cylindre. 

![](images/bouton3D.png)

Dans l'ordre, j'ai d'abord établi une esquisse de ce que je voulais; un cercle un premier lieu. Pour cela, j'ai crée un nouveau fichier (que j'appelle essay).

![](images/new.png)

Ensuite, il faut aller sur **part design** afin de créer un **body** (create a body) pour ensuite créer une **esquisse** ("create sketch"). Il faut ensuite désigner le plan sur lequel on souhaitre travailler. Enfin, on peut commencer !! 

![](images/partDesign.png)


J'ai donc commencé par créer un cercle centré en appuyant sur la touche **circle** (un cercle avec une boule rouge au centre) 

![](images/circle.png)

Par contre, je n'ai pas centré mon cercle, alors je supprime ce que j'ai crée et pour ça, je selectionne la commande "**delete**" la contrainte 1 qui est donc le cercle. 

![](images/delete.png)

De plus, je mets une contrainte sur le rayon du cercle: 

![](images/contrainteR.png)

Enfin, je vais dans **spreadsheet** (dans la barre de recherche en haut de la page), cette spreadsheet me permet d'insérer des variables ainsi que leurs valeurs de sorte à ce que si je veux modifier la taille d'une de mes variables, je peux le faire directement dans le spreadsheet, sans devoir modifier dans le cercle de départ. 

![](images/spreadsheet.png)

Ensuite il suffit d'appuyer sur cette touche, - une fois que tous les degrés de liberté sont "réglés", il devrait être marque "all constraint" dans le sketch - afin de compléter le modèle en 3D : 

![](images/pad.png)

Et voilà !!! J'ai enfin crée mon cylindre ! 

PS: j'ai découvert quelques jours plus tard une vidéo expliquant comment faire un creux dans les formes créées à partir de FreeCAD, je te laisse avec ce petit [tuto](https://www.youtube.com/watch?v=kETP-hruThE&t=73s) pour débutant. 



## 2.2 Conception personnelle 


Plus tard, j'ai essayé de mon côté de faire une pièce géomètrique en 3D qui était plus "complexe" pour me permettre de plus me familiariser avec **OpenSCAD**. De plus, au sein du cours nous avons vu qu'est ce que des [flexlinks](https://www.compliantmechanisms.byu.edu/flexlinks). 

J'ai eu beaucoup de mal à réellement comprendre qu'est ce que c'est; j'ai trouvé sur internet que c'est "un mécanisme souple, flexible qui transmet la force et le mouvement par la déformation élastique du corps. Il tire une partie ou la totalité de son mouvement de la flexibilité relative de ses membres plutôt que des seules articulations du corps rigide"

Du coup, j'ai d'abord essayé plusieurs fois à en créer un toute seule. Je ne suis pas arrivée à coder directement toute seule. J'ai donc consulté les différentes sites de mes camarades et avec l'aide de [Kawtar](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/Kawtar.zaouia/), j'ai réussi à créer un objet plus ou moins correct qui a l'air d'un flexlinks.

![](images/flexlink.png)

Voici le code que j'ai utilisé pour arriver à cette fin: 

![](images/code.png)


~~~
//https://creativecommons.org/licenses/by/4.0/
// cet objet a été créé par Mariam Mekray et il se trouve sur CC BY 4.0
$fn = 100;
// facet number = nombre de "mini faces" qui vont constituer mes demi-sphères 
// j'en mets un maximum pour permettre qu'elle soit la plus lisse possible

height = 4;
//hauteur du cylindre 
radius = 3;
// rayon du cylindre 
d = 10; 
//distance entre mes deux cylindres de base
L = 30;
// distance de séparation entre mes deux bases de l'objet
e = 1; 
// épaisseur 
x = 2;
// nombre de cylindre qui vont fusionner 
b = 1; 
// épaisseur des bordures des cylindre de base


module base(){
    //crée une base de cylindre 
    difference(){
        
        hull(){
            // on crée un cylindre, ensuite on crée un deuxième en translatant le premier
            // grâce à la fonction  hull on fait fusionner les deux pour créer la base 
            cylinder(h = height, r= radius + b, center = true); 
            // création du cylindre de base qui va se translater et ensuiste fusionner avec l'autre 
            translate([(x-1)*d, 0, 0]){cylinder(h = height, r= radius + b, center = true);}
            //cette translation permet au cylindre de base de se "copier" 
        }
            cylinder(h = height, r= radius, center = true); 
            translate([(x-1)*d,0,0]){cylinder(h = height, r= radius, center = true);}
    }
}
base ();

translate([(-d)-(2*radius)-(2*b) -L,0,0]){
    // translation de la abse pour créer une deuxième base 
    base();
}
translate([-L+d+b,(-e/2),0])
cube([L, e, height], center = true); 
// création de la barre de soutien qui supporte les deux bases
~~~

Tout d'abord, j'ai décidé de créer une "base". La base est la structure qui comporte les deux trous cylindriques. Pour cela, j'ai d'abord créé un cylindre, que j'ai ensuite translaté pour afin qu'il y ait deux cylindre. C'est comme une "copie" de mon cylindre de base. Ensuite, il fallait que je fusionne mes deux cylindre pour créer la grande structure : "**la base**". 

La fonction **difference** va ensuite me permettre de créer les trous. Pour cela, je soustrais les dimensions des cylindres "trous" du cylindre de base (et cela se fait pour les deux cylindre qui fusionnent). 

Une fois la base créée avec les trous, il faut la translater (**translate**) pour créer deux bases de la structure. 

Finalement, la fonction **cube** m'a permis de construire la barre de jonction entre mes deux bases. 

Et voilà pour mon flexlinks ! 



































