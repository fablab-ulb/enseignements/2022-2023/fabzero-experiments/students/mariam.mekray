# 3. Impression 3D


Cette semaine, nous avons suivi une formation de 2 heures au fablab sur l'impression 3D et comment utiliser les imprimantes 3D. Tout d'abord, pour cela nous avons commencé par télécharger PrusaSlicer, un logiciel qui permet l'impression de nos differentes créations sur FreeCAD, OpenSCAD,... 


Tout d'abord, à quoi cela servirait-il?  Comme dit dans le module précédent, cette technologie est utilisée dans différents secteurs comme  en mécanique, en médecine, en architecture, en machinerie industrielle, et j'en passe. En plus, elle s'avère très utilse dans la création de prototype et dans la production. 


## 3.1 Quels sont les avantages? 


Tout d'abord, cette révolution nous permet d'imprimer toutes sortes d'objets. De plus, cette technique est rapide et permet d'imprimer un objet en quelques minutes jusqu'à même des heures. Tout dépend de la complexité. Ensuite, un autre avantage est la modification d'objets, de constructions, de certains défauts, etc en peu de temps. Il permet aussi la visualisation sans réelles contraintes qui peuvent être rencontrées dans les méthodes de fabrications traditionnelles. 


## 3.2 Téléchargement de PrusaSlicer 

Pour commencer, l'installation de ce logiciel peut se faire à partir de ce [lien](https://www.prusa3d.com/fr/page/prusaslicer_424/). 
PrusaSlicer est une application permettant d'imprimer nos modèle 3D. Pour savoir l'utiliser, j'ai consulté [cette page web](https://gitlab.com/fablab-ulb/enseignements/fabzero/3d-print/-/blob/main/3D-printers.md) du cours. 




## 3.3 Mon modèle sur PrusaSlicer

Pour ouvrir mon modèle 3D sur PrusaSlicer, il faut l'enregistrer en format .stl. Ensuite, je l'ai tout simplement téléchargé dans le Finder et j'ai suivi les étapes du tutoriel qui figurent plus haut. 

Une fois que tout les réglages étaient finis, j'ai sélectionné la touche "découper maintenant". 

![](images/découper.png)

Je peux en plus vérifier le temps d'impression dans la fenêtre orange qui s'ouvre à gauche de mon fichier. Enfin, il faut exporter le fichier dans "exporter le G-code" toujours en bas de page à droite pour permettre l'insertion du fichier dans une carte sd qui va ensuite être insérée à son tour dans l'imprimante Prusa. 

Ma petite création est censée se compléter avec celles de [Sami](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/sami.el.hamdou/), [Kamel](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/kamel.sanou/) et [Serge](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/kodjo.dao/). 

![](images/oblique.png) ![](images/haut.png)


Quelques semaines plus tard avec le groupe nous avions décidé de recommencer la conception de notre catapulte mais avec les bonnes dimensions. Nous nous sommes alors réunis et nous avions modifié les dimensiosn afin qu'elles concordent.

Voici une image finale de la catapulte désirée: 

![](images/newcata.png)



























