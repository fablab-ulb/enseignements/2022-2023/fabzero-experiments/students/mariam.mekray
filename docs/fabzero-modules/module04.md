# 4 Outil de formation Fablab 

Cette semaine, nous devions nous inscrire à une formation parmis 4 formations qui sont les suivantes: 
- laser cutter 
- microcontrôle 
- freineuse CNC 
- réparabilité et impression 3D 

Pamis ces dernières, j'ai décidé de choisir **laser cutter** car j'avais déjà lu que la découpe laser était une découpe très efficace et très avantageuse pour les production rapide mais aussi car avec mon groupe nous avsions décidé de choisir des formations différente pour nous permettre de nous diversifier. 

De plus, il existe plusieurs matériaux sur lesquelles nous pouvons effectuer des découpes. Nous avions vu durant la formation qu'il était possible de découper ou de graver sur PE, PET, MDF, acrylique, mais aussi sur du carton ou du POM. 

Continuons sur les avantages de cette méthode. C'est en effet une technique d'une très grande précision même pour les découpes les plus complexes ou avec le plus de détails. 

Il est en plus possible de graver tout ce que l'on désire sur notre matériel, que ce soit un dessin, une image, un slogan, mais aussi des zones plus ombrés que d'autres pour créer des contrastes. Les différentes gravures ainsi que les découpes peuvent être faites avec différentes profondeur. 

## 4.1 Informations sur les découpeuses 

Durant la formation, nous avions appris à manipuler les 3 machines différentes; **Epilog Fusion Pro**, **Full Spectrum Muse** ainsi que **Lasersaur**. 

### 4.1.1 Epilog Fusion Pro 32
[Ici](https://gitlab.com/fablab-ulb/enseignements/fabzero/laser-cut/-/blob/main/Epilog.md) il est possible de trouver plus d'informations sur l'utilisation smais aussi toutes les caractéristiques de cette machine. 

C'est l'une des deux machines les plus puissantes du Fablab (avec Lasersaur). Elle a une grande surface de découpe par rapport à la Full Spectrum Muse. C'est la machine "la plus utilisée", en tout cas c'est celle que l'on nous a conesillé d'utiliser de base si nous avions le choix. 

![](images/epilog.png)


### 4.1.2 Full Spectrum Muse 

Cette machine possédait une surface de découpe beaucoup plus petite que les deux autres. De plus elle est moins puissante. Cependant, il est possible de connecter son pc directement au serveur de la découpeuse via Wi-Fi. 

Cet avantage est crucial pour les personnes comme moi, qui ont un Mac et qui n'ont pas d'adaptateur USB sur place, ça devient alors compliqué d'exporter son fichier vers une des découpeuse. 

![](images/spectrum.png)

### 4.1.3 Lasersaur 

Cette machine était un peu partticulière car elle était faite "maison" c'est-à-dire qu'elle a été conçue au Fablab. Néanmoins, elle est très efficace, elle a un etrès grande surface de découpe et est très puissante. 

Durant la séance, j'ai assisté à la découpe de deux étudiantes avec cette machine. Il fallait absolument allumer la vanne d'air comprimé, l'extracteur de fumée et le refroidisseur sans quoi la découpe ne pouvait pas débuter car il y avait un témoin qui s'allumait pour avertir que ces étapes n'ont pas toutes été faites. 

Vous trouverez [ici](https://gitlab.com/fablab-ulb/enseignements/fabzero/laser-cut/-/blob/main/Lasersaur.md) un guide d'utilisation. 

![](images/lasersaur.png)

## 4.2 Mesures indispensables 

L'assitant chargé de nous donner la formation nous a introduit les différentes précautions à prendre en compte pour notre sécurité mais aussi afin d'éviter de provoquer un incendie. 

Pour cela, au début de la séance il était important de passer un quizz dans lequel j'ai appris que : 
- il faut toujours rester à proximité de la machine (sauf si nous devions aller au toilette)
- toujours allumer le filtre (sinon risque de fumée et d'incendie)
- activer l'extracteur de fumée (pour certaines machines)
- appuyer sur le bouton d'arrêt d'urgence si il y a une grosse apparition de fumée anormale. 
- ne pas ouvrir la machine si ma découpe n'est pas finie 


## 4.3 Mise au point du fonctionnement 

Ensuite, il faut savoir que nous pouvons jouer avec la **puissance** et la **vitesse**. Ce sont les deux facteurs qui vont permettre des modofication dans la découpe ou la gravure. Par exemple, lors de ma première découpe, la vitesse choisie était plutôt **faible** et la puissance **élevée**, alors on pouvait observer de la fumée. Pour cela je pouvais soit augmenter la vitesse ou la puissance. 

De plus, en fonction de la puisance, le laser va découper ou graver. Si la puissance est plutôt faible, le laser va seulement graver car il ne va pas passer à travers le matériau mais cela dépend aussi du matériau choisi. 

Maintenant, je vous laisse avec une [page](https://gitlab.com/fablab-ulb/enseignements/fabzero/laser-cut/-/blob/main/Laser-cutters.md) qui contient encore plus d'informations nécéssaires que celles que je viens d'énoncer. 


## 4.4 Ma création 

Pour commencer, j'ai d'abord téléharger ce [site](https://www.festi.info/boxes.py/). 
Il propose plusieurs boîtes mais aussi des divers assemblages que l'on peut manipuler et paramétrer selon ce qu'on souhiate avoir comme figure finale. Ensuite, il suffit de télecharger le fichier, et de l'ouvrir sur [Inkscape](https://inkscape.org) pour modifier la boite. Il est important, en étape finale de télécharger le fichier en format **.svg**. Petite précision, sur la machine Full spectrum Muse, il fallait télécharger le fichier en format .htc  si c'était une image, sinon la découpe ne fonctionnait pas. 

![boxespy](images/boxespy.png)

Comme on peut le voir sur l'image j'ai changé les paramètre de taille; je voulais une boite carrée alors toutes mes "arrêtes" avaient une longueur de 50 mm. 


Pour passer à la découpe, il faut se connecter au réseau wi-fi de la découpeuse, ainsi il est plus facile pour mon pc car je n'ai pas besoin d'USB. Ensuite, il suffit d'entrer un code dans la barre de recherche et d'accéder au paramètres pour permettre le découpage. 

Ensuite, il faut calibrer la découpeuse sinon, il y a des zones qui sont hors de la zone de découpage. Pour cela, il y a des zones en couleur **verte**, ceci veut dire que notre création fait partie du plan où se trouve la "planche" (que nous allons découper). Mais il peut aussi avoir des zones rouges, cela veut simplement dire que c'est hors-zone du plan de découpe. en effet, le laser en découpant fait des aller-retours à une vitesse constante sur une zone et donc il lui faut un petit espace de "freinage" ou de "démarrage". 

Il est primordial de jouer avec les paramètres de puissance et de vitesse pour permettre à la machine de graver ou de découper. J'ai utilisé une puissance de 40 et une vitesse de 100. À la fin du premier passage, j'ai remarqué que la machine a gravé même les zones où je voulais qu'elle découpe. Donc pour qu'elle découpe il fallait que j'emploie une vitesse plus petite et une puissance plus élevée. Mais à la place j'ai juste décidé de repasser sur la gravure plusieurs fois (2 fois) jusqu'à ce que ça découpe. Seulement, pour ne pas découper ma zone de gravure aussi, j'ai juste mis un 0 dans la section passage de la gravure. 

Petite info, les vitesses et puissances qui sont utilisés en générale pour gravure ou découpe sont préalablement testé par les assistants du Fablab. D'ailleurs, c'est l'assistant de la formation qui nous a indiqué quelles vitesses et puissances utiliser. Maintenant, je sais qu'il vaut mieux effectuer des tests à l'avance avant de passer à la découpe pour qu'elle soit idéale (pareil pour gravure). 

Je ne pourrai pas partager des images des différents paramètres à manipuler car mon pc ne voulait pas se connecter sur le réseau de la machine. J'ai donc utilisé le pc de notre assistant. 

Une fois les deux passages finis, il a fallu que j'assemble les différentes pièces de mon cube. 

![cube](images/cube.png)     ![cube1](images/cube1.png)

![cube2](images/cube2.png)






