# 1. Documentation 

Durant le premier cours, les techniques et les différentes méthode nous ont été introduites afin d'avoir une bonne documentation. 

## 1.1 Pourquoi écrire une documentation? 

La documentation me permet de revenir et d'avoir une vue d'ensemble sur mon travail. Il est d'autant plus important d'avoir à disposition la documentation d'autres étudiants des années précédentes afin de pouvoir chercher des réponses à différentes problèmatiques rencontrées tout au long du projet et qui sont inspirant. 

Cette section permettra de revenir sur mes réalisations, sur toutes les étapes pour aboutir à mon projet, sur les problèmes rencontrés avec mon pc mais aussi tous les avantages que présente MAC qui m'ont permis de réaliser certaines tâches plus facilement.

## 1.2 Quels sont les différentes outils utilisés? 

Comme dit plus haut, j'ai un Macbook, je n'ai donc pas du télécharger CLI comme l'ont fait d'autres élèves (LE SEUL AVANTAGE :/). Sur macOs, il y a déjà un terminal de commandes. 

![](images/terminal.png)

De plus, nous avons également utilisé Git, VScode, GraphicsMagick, MKdocs, ...

### 1.2.1 Les bases des commandes de bases: 

Ici, ce sont les commandes de bases que j'ai appris pour pouvoir utiliser le terminal et lui demander d'éxecuter les commandes que j'avais besoin. 

| Commande|  Description|  
|-----|-----------------|
| ls  | liste des dossiers dans le dépot|             
| cd  | naviguer entre les dossiers|
|mkdir nom du fichier| créer un fichier|
| git clone | clôner un dossier |
| git add ou git add *| ajoute toutes les modifcations apportés aux fichiers ou à tous les fichiers |
| git commit -m| permet de commenter nos modifications |
| git push | envoie la dernière version du projet au serveur général|
| git pull | sauvegarder la dernière version du projet |
| gm convert -resize 600x600 NomDeImage.png(ou jpg)| permet de convertir une image en un format moins volumineux | 


## 1.3 À propos de git 

Git est un outil nous permettant de travailler en collaboration sur un même projet avec d'autres étudiants. De plus, cet outil nous permet de voir les modifications apportées au projet en les  sauvegardant tout en nous permettant de revenir sur nos anciennes versions. 

En plus, il nous est possible d'enregistrer des copies du projet sur notre pc afin de laisser des traces et de nous permettre de les modifier. 

### 1.3.1 L'installation de gitlab 

Afin d'installer, il suffit d'aller sur ce [site](https://git-scm.com/) et installer Gitlab. Ensuite, pour travailler sur le projet sur gitlab, il faudrait s'identifier et pour cela il faut une clé SSH. Cette clé permet de nous identifier ainsi que de travailler sans avoir à se connecter tout le temps. 

Pour vérifier quelle version de git il s'agit, il suffit d'écrire la commande "git --version" sur le terminal de commande. 

![](images/imageGitVersion.png )

Ensuite, se suit l'étape de configuration de mon email et mon username sur Gitlab. Pour cela, il suffit de d'écrire sur le terminal la commande  git config --global user.email "prénom.nom@ulb.be" ainsi que "git config --global user.name "prénom.nom". 

## 1.4 La clé SSH 

Maintenant que Git est configuré, il faut créer une clé SSH qui permet de créer une connexion sécurisée. Il existe deux clés; une publique ainsi qu'une privée.  La clé publique sert à crypter des informations elle est ainsi utilisée par le système utilisateur et le système distant. La clé privée est unique au système de l'utilisateur et permet l'identification pour la communication avec le système à distance. Cette clé possède un mot de passe qui est privé. 

Pour cela, j'ai tappé la commande suivante sur le terminal:

![](images/sshkeygen.png)

Durant cette opération le terminal demande d'entrer le nom d'un fichier afin de sauvegarder ma clé dessus. J'ai seulement appuyé sur ma touche "Entrée" afin de la retrouver plus facilement. Ensuite, il faut entrer un "passphrase" qui est un mot de passe qui reste secret. 

Enfin, j'ai fait une erreur en tappant dans le terminal la commande suivante qui permet d'ajouter la clé SSH publique au compte Gitlab. Donc pour cela il faut d'abord copier la clé publique: 

![](images/keypub.pngeypub.png)

Seulement, cette commande ne donnait aucun résultat car dans un premier temps ce n'est pas la bonne commande. La commande pour macOs est :

![](images/pbcopy.png))



Ensuite, il faut trouver la clé dans le fichier "mariam.mekray" dans mon pc. Comme sur macOs les fichiers sont parfois chachés, j'ai tappé "shift+cmd+G" dans le finder afin de trouver l'extension /.ssh et donc mes deux clés dont la publique. 

![](images/ided.png)


Je vous passe une série d'opérations qui ont mené à l'échec car je copiais toujours la mauvaise clé puisque je ne savais pas qu'il existait des fichiers cachés. Et comme on peut le voir sur l'image, il y avait sans cesse cette réponse "permission denied". 

![](images/permission.png)

L'étape suivant consiste à copier la clé publique dans l'emplacement prévu sur le site fablab du cours dans l'espace "key". Une fois copié, il faut cloner l'espace de travail grâce à la clé avec ssh comme indiqué sur l'image suivante: 

![](images/clone.png)

Enfin, il est maintenant possible de travailler sur le projet à partir de notre ordinateur sur la copie du projet. 
Pour cela, il faut maitriser les commandes gitlab inscrites dans le tableau plus haut. Ces commandes permettent d'ajouter des modifcations au projet, de les télécharger et de les envoyer sur le serveur général. 

## 1.5 Markdown

Markdown est le format avec lequel nous allons "coder" qu'ensuite nous allons exporter sous un autre format (HTML). C'est un language qui permet de rédiger des textes sur Internet ou même fors ligne. Pour cela il faut utiliser un éditeur de texte tel que VScode. Cet outil nous permettra d'écrire ce que nous souhaitons afficher dans notre documentation et de l'exporter sous format HTML. 

### 1.5.1 Visual Studio Code 

Il suffit simplement de le télécharger [ici](https://code.visualstudio.com). Cet éditeur permet de modifier les fichiers qui se trouvent dans le fichier "prénom.nom" dans le Finder. Ici, c'est ce que je suis souvent amenée à faire en tout cas. Il est important d'ajouter ".md" aux titres que j'accorde à mes fichiers. 

De plus, j'ai écrit une commande qui permet de télécharger la language python sur mon pc. Pour cela j'ai d'abord installé [python](https://www.python.org). Ensuite, j'ai tappé la commande "python --version" pour savoir la version que j'ai téléchargé et pour être sûr qu'il a réellement été installé. 

Enfin, j'ai tappé les commandes suivantes pour téléchragé mkdocs : 

![](images/mkdocsnew.png)



### 1.5.2 Commandes spéciales dans VSCode 

| Commande|  Description|  
|-----|-----------------|
| # | permet d'inséer un titre|             
| ## | permet d'inséer un sous-titre|
| ### |permet d'inséer un sous-sous-titre|
| [texte](lien) | ajouter un lien |
| ![nom](images/NomDeImage.png| ajouter une image|
| ** ** | permet d'écrire en gras|

### 1.5.3 Modification du site 

Enfin, il existe deux chemins différents que j'utilise pour accéder aux fichiers sur VSCode. 
Le premier : je vais dans le Finder. Je vais sur mon dossier "mariam.mekray" > Docs > Fabzero-modules > je peux ensuite sélectionner n'importe quel module, ce dernier s'ouvre automatiquement sur VSCode. 

Le deuxième : aller sur [le site de notre cours sur Gitlab] (https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments) pour ensuite aller sur [students](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments). enfin sur [mariam.mekray](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/mariam.mekray), je peux accéder à [docs](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/mariam.mekray/-/tree/main/docs) puis sur [fabzero-modules](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/mariam.mekray/-/tree/main/docs/fabzero-modules), et voilà l'accès à tous les différents modules et l'index. 

![](images/proof.png)

## 1.6 Graphic magicks 

Enfin, pour convertir ou réduire et donner une taille précise à vos images il vous siffit de télécharger [Graphic Magicks](http://www.graphicsmagick.org/download.html). 
Grâce à Graphic magicks, la commande que j'ai noté plus haut pour convertir la taille d'une image (**resize**)  pourra fonctionner sur le terminal de commande.
Cette fonction permet de réduire sa taille avant de l'uploader sur VSCode. 



































