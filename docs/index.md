
## Avant-propos 
Bienvenue sur mon site cher visiteur ! 

## Qui suis-je ? 
![](img/moi.png)

Je m'appelle Mariam, j'ai 20 ans et je suis étudiante en BA3 en bioingénieur à l'ULB. Cette année je suis le cours de “How To Make (almost) Any Experiments Using Digital Fabrication”. 

Lorsque je ne travaille pas, j'aime passer du temps avec ma famille mes amis, regarder des matchs de football ou de basketball à la télé. Quand je veux chiller, je regarde une série ou des films. 

De plus, plusieurs fois dans la semaine, je participe à des assises où nous discutons de religion, afin d'augmenter mon savoir sur ma religion. Il m'arrive aussi d'aller marcher car j'aime aussi passer beaucoup de temps seule à méditer. 

Enfin, j'aime beaucoup voyager et découvrir de nouveaux horizons et de nouveaux continents. 

## Mon background 
J'ai choisi l'option de bioingénieur car je ne savais pas trop quoi choisir, en plus ma prof de sciences de secondaire m'avait conseillé de faire des études de bioingénieur. Pour l'instant je ne regrette pas d'avoir pris cette option sauf pendant le blocus lol. 

## Mes travaux précédents 
Je n'ai pas de travaux bien intéressants mis à part le projet auquel je participe qui est expo. Cette année nous abordons La biodiversité de La Grande Barrière de Corail. Nous avons fait une maquette sur cette barrière de corail. 


![](img/maquette.png)  ![](img/stand.png)

Merci pour ton intérêt, 
Bonne journée ! 













