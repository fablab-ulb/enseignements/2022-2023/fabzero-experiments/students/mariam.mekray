## Foreword

Hello!

This is an example student blog for the [2022-2023 ULB class "How To Make (almost) Any Experiments Using Digital Fabrication"](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/class-website/).

Each student has a personal GitLab remote repository in the [GitLab class group](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students) in which a template of a blog is already stored. 

The GitLab software  is set to use [MkDocs](https://www.mkdocs.org/) to turn simple text files written in [Markdown](https://en.wikipedia.org/wiki/Markdown) format, into the site you are navigating.

#### Edit your blog

There are several ways to edit your blog

* by navigating your remote GitLab repository and [editing the files using the GitLab Web Editor](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#edit-a-file).
* by using [git](https://rogerdudler.github.io/git-guide/) and Command Line on your computer : cloning a local copy of your online project repository, editing the files locally and synchronizing back your local repository with the copy on the remote server.

#### Publishing your blog

Two times a week and each time you change a file, the site is rebuilt and all the changes are published in few minutes.

#### It saves history

No worries, you can't break anything, all the changes you make are saved under [Version Control](https://en.wikipedia.org/wiki/Version_control) using [GIT](https://git-scm.com/book/en/v2/Getting-Started-About-Version-Control). This means that you have all the different versions of your page saved and available all the time in the Gitlab interface.

## About me

![](images/avatar-photo.jpg)

Hi! I am Emma Brewer. I am an art director & graphic designer based in New York City working on branding, visual identities, editorial design and web design.

Visit this website to see my work!

## My background

I was born in a nice city called..

## Previous work

I'm a paragraph. Edit the page on Gitlab to add your own text and edit me.  I’m a great place for you to tell a story and let your users know a little more about you.​

### Project A

This is an image from an external site:

![This is the image caption](https://images.unsplash.com/photo-1512436991641-6745cdb1723f?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=ad25f4eb5444edddb0c5fb252a7f1dce&auto=format&fit=crop&w=900&q=80)

While this is an image from the assets/images folder. Never use absolute paths (starting with /) when linking local images, always relative.

![This is another caption](images/sample-photo.jpg)





## 5.3 
## 5.3.1 Arbres de problèmes et de solutions 

Une troisième séance en groupe se présente, mais je ne suis pas résente à cette séance-là malheureusement. 
Cependant, je demande au groupe de me faire un feedback: 

Nous étions deux membres du groipe à être absents mais le prof a demandé d'avancer malgré ça. Tout d'abord, il fallait converger vers un sujet de problématique car notre problématique était encore trop globale. Pour ce faire, chacun d'entre eux a d'abord écrit sur une affiche les sujets en rapport avec notre problématique globale mais qui leur plaisent la plus. 

Sur base de leurs différentes idées, il fallait revoir les similitudes entre les différents sujets proposés par chacuns. Celui qui ressortait le plus était la **biodiversité**, plus précisément la **perte de biodiversité**. 

![](images/nuage.png)

En effet, la perte de biodiversité est dûe à de multiples causes comme : 
- le changement climatique 
- le changement d'usage des terres et des océans (ou des mers) 
- déclin de certaines population pollinisatrices (plus de reproduction) 
- espèces exotiques envahissantes 
- déclin de populations dûe aux polluants chimiques envoyés vers l'atmosphère 

D'ailleurs, de mon côté j'ai regardé plusieurs vidéos ou documentaires sur la perte de biodiversité suite à la pollution mais aussi perte de biodiversité en générale causée par l'homme. Voici quelques liens toujours utiles et qui vous permettront de vous renseigner mais aussi de mesurer l'impact que peuvent avoir nos actions sur l'environnement mais l'importance de la biodiversité terrestre, animale et végétale pour nous, êtres humains. 

- https://www.youtube.com/watch?v=wD091fJMeU4
- https://www.youtube.com/watch?v=JjgQqm8EwK4
- https://www.arte.tv/fr/videos/098713-001-A/data-science/

Par rapport à ça, je suis contente que mon groupe s'est  plutôt dirigé vers ce sujet car c'est une problématique qui  me tient vraiment à coeur. De plus je trouve qu'en général l'homme est assez inconscient et endormi face au danger de cette perte de biodiversité. 

Ensuite, il y fallait se diriger vers une cause de perte de biodiversité plus "précise", mon groupe a alors convergé vers la pollution des eaux. Concernant la pollution des eaux, il est interéssant de connaitre ou d'apprendre à connaitre les impacts que nous humains avons sur les eaux et leur importance et l'importance de la biodiversité qu'ils abritent. L'écosystème marin n'est pas "connu" comme le terrestre. Donc, je pense que nous avons du mal à imaginer l'importance pour nous qu'a chaque être vivant ou non-vivant dans les eaux. 
C'est alors un sujet excellent selon moi. 

De plus, cela me touche car comme dit dans l'[index](), j'ai participé à l'exposition des sciences 2023 et le sujet que j'ai presenté portait sur la Grande Barrière de Corail. En approfondissant mes recherches, j'ai d'abord découvert les extinctions de masses qu'a déjà vécu cette barrière de corail mais j'ai surtout appris l'importance de cette barrière à l'écosystème marin. C'est sans négliger le rôle  significatif qu'elle a pour les humains. Je ne vous laisse donc pas imaginer l'impact néfaste de la pollution des eaux qui mène à de grosses extinctions de masse, et si ces barrières de corail et tous les être vivants qu'elles abritent disparaisssent. Les dégats seraient sans précédent. 

Revenons à l'essentiel, mon groupe devait ensuite réfélchir à quelles types de pollutions, leurs causes et leurs conséquences. Tout cela devait se faire sous forme d'abre. 

Enfin, c'était encore trop large comme idées alors il leur était conseillé de mettre une contrainte sur le choix de leur problematique. 

![](images/arbre.png)


Ils se sont alors un peu plus renseigné pendant le cours et ont lu que la Senne était l'une des rivières les plus polluées de Belgique. En plus, il y a une grande perte de biodiversité. 






























 


















# 2. Conception Assistée par Ordinateur (CAO)

This week I worked on defining my final project idea and started to getting used to the documentation process.

## title 1

"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."

### subtitle 1

"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."


## title 2

"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.

## Table 1

| Qty |  Description    |  Price  |           Link           | Notes  |
|-----|-----------------|---------|--------------------------|--------|
| 1   | Material one    |  22.00 $| http://amazon.com/test   |    Order many    |
| 1   | Material two    |  22.00 $| http://amazon.com/test   |        |
| 1   | Material three  |  22.00 $| http://amazon.com/test   |        |
| 1   | Material five   |  22.00 $| http://amazon.com/test   |        |
| 1   | Material eight  |  22.00 $| http://amazon.com/test   |        |
| 1   | Material twelve |  22.00 $| http://amazon.com/test   |        |
| 1   | Material eleven |  22.00 $| http://amazon.com/test   |        |

## Useful links

- [Docsify](https://docsify.js.org/#/)
- [Markdown](https://en.wikipedia.org/wiki/Markdown)


## Code Example

Use the three backticks to separate code.

```
// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);                       // wait for a second
}
```


## Image gallery

![](../images/fablab-machine-logos.svg)

## Video

### From Vimeo

<iframe src="https://player.vimeo.com/video/10048961" width="640" height="480" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
<p><a href="https://vimeo.com/10048961">Sound Waves</a> from <a href="https://vimeo.com/radarboy">George Gally (Radarboy)</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

### From Youtube

<iframe width="560" height="315" src="https://www.youtube.com/embed/Q3oItpVa9fs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## 3D Models

<div class="sketchfab-embed-wrapper"><iframe width="640" height="480" src="https://sketchfab.com/models/658c8f8a2f3042c3ad7bdedd83f1c915/embed" frameborder="0" allow="autoplay; fullscreen; vr" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

<p style="font-size: 13px; font-weight: normal; margin: 5px; color: #4A4A4A;">
    <a href="https://sketchfab.com/models/658c8f8a2f3042c3ad7bdedd83f1c915?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Dita&#39;s Gown</a>
    by <a href="https://sketchfab.com/francisbitontistudio?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Francis Bitonti Studio</a>
    on <a href="https://sketchfab.com?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Sketchfab</a>
</p>
</div>



















his week I worked on defining my final project idea and started to getting used to the documentation process.

## title 1

"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."

### subtitle 1

"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."


## title 2

"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.

## Table 1

| Qty |  Description    |  Price  |           Link           | Notes  |
|-----|-----------------|---------|--------------------------|--------|
| 1   | Material one    |  22.00 $| http://amazon.com/test   |    Order many    |
| 1   | Material two    |  22.00 $| http://amazon.com/test   |        |
| 1   | Material three  |  22.00 $| http://amazon.com/test   |        |
| 1   | Material five   |  22.00 $| http://amazon.com/test   |        |
| 1   | Material eight  |  22.00 $| http://amazon.com/test   |        |
| 1   | Material twelve |  22.00 $| http://amazon.com/test   |        |
| 1   | Material eleven |  22.00 $| http://amazon.com/test   |        |

## Useful links

- [Docsify](https://docsify.js.org/#/)
- [Markdown](https://en.wikipedia.org/wiki/Markdown)


## Code Example

Use the three backticks to separate code.

```
// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);                       // wait for a second
}
```


## Image gallery

![](../images/fablab-machine-logos.svg)

## Video

### From Vimeo

<iframe src="https://player.vimeo.com/video/10048961" width="640" height="480" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
<p><a href="https://vimeo.com/10048961">Sound Waves</a> from <a href="https://vimeo.com/radarboy">George Gally (Radarboy)</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

### From Youtube

<iframe width="560" height="315" src="https://www.youtube.com/embed/Q3oItpVa9fs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## 3D Models

<div class="sketchfab-embed-wrapper"><iframe width="640" height="480" src="https://sketchfab.com/models/658c8f8a2f3042c3ad7bdedd83f1c915/embed" frameborder="0" allow="autoplay; fullscreen; vr" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

<p style="font-size: 13px; font-weight: normal; margin: 5px; color: #4A4A4A;">
    <a href="https://sketchfab.com/models/658c8f8a2f3042c3ad7bdedd83f1c915?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Dita&#39;s Gown</a>
    by <a href="https://sketchfab.com/francisbitontistudio?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Francis Bitonti Studio</a>
    on <a href="https://sketchfab.com?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Sketchfab</a>
</p>
</div>
his week I worked on defining my final project idea and started to getting used to the documentation process.

## title 1

"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."

### subtitle 1

"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."


## title 2

"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.

## Table 1

| Qty |  Description    |  Price  |           Link           | Notes  |
|-----|-----------------|---------|--------------------------|--------|
| 1   | Material one    |  22.00 $| http://amazon.com/test   |    Order many    |
| 1   | Material two    |  22.00 $| http://amazon.com/test   |        |
| 1   | Material three  |  22.00 $| http://amazon.com/test   |        |
| 1   | Material five   |  22.00 $| http://amazon.com/test   |        |
| 1   | Material eight  |  22.00 $| http://amazon.com/test   |        |
| 1   | Material twelve |  22.00 $| http://amazon.com/test   |        |
| 1   | Material eleven |  22.00 $| http://amazon.com/test   |        |

## Useful links

- [Docsify](https://docsify.js.org/#/)
- [Markdown](https://en.wikipedia.org/wiki/Markdown)


## Code Example

Use the three backticks to separate code.

```
// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);                       // wait for a second
}
```


## Image gallery

![](../images/fablab-machine-logos.svg)

## Video

### From Vimeo

<iframe src="https://player.vimeo.com/video/10048961" width="640" height="480" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
<p><a href="https://vimeo.com/10048961">Sound Waves</a> from <a href="https://vimeo.com/radarboy">George Gally (Radarboy)</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

### From Youtube

<iframe width="560" height="315" src="https://www.youtube.com/embed/Q3oItpVa9fs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## 3D Models

<div class="sketchfab-embed-wrapper"><iframe width="640" height="480" src="https://sketchfab.com/models/658c8f8a2f3042c3ad7bdedd83f1c915/embed" frameborder="0" allow="autoplay; fullscreen; vr" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

<p style="font-size: 13px; font-weight: normal; margin: 5px; color: #4A4A4A;">
    <a href="https://sketchfab.com/models/658c8f8a2f3042c3ad7bdedd83f1c915?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Dita&#39;s Gown</a>
    by <a href="https://sketchfab.com/francisbitontistudio?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Francis Bitonti Studio</a>
    on <a href="https://sketchfab.com?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Sketchfab</a>
</p>
</div>




